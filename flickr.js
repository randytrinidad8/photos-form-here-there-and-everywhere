var x = document.getElementById("geoLocation");
var geoLocation={
    Latitude: 33.74993,
    Longitude: 83.98769
}
//gets your location
function yourLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(gotLocation,failedError);
  } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}

function gotLocation(position) {
    geoLocation.Latitude=position.coords.latitude;
    geoLocation.Longitude=position.coords.longitude;

  x.innerHTML = "Latitude: " +geoLocation.Latitude +
  "<br>Longitude: " + geoLocation.Longitude;
  photoFromFlickr();
}

function failedError() {
    photoFromFlickr();
}

let flickr={
    Key:'993495cc1b2d41eddc4877dd30988557',
    Secret:'a165a5fdb9b97cdb'
}

let photos=[];
let currentIndex=0;

function photoFromFlickr(){
    const linkFlickr=`https://cors-anywhere.herokuapp.com/https://flickr.com/services/rest/?api_key=${flickr.Key}&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=${geoLocation.Latitude}&lon=${geoLocation.Longitude}&text=shop`;
    //const linkFlickr=`https://flickr.com/services/rest/?api_key=${flickr.Key}&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=${geoLocation.Latitude}&lon=${geoLocation.Longitude}&text=shop`;

    fetch(linkFlickr)
  .then(responseObject => responseObject.json())
  .then(data => {
    data.photos.photo.forEach(element => {
        const imageUrl = genURL(element);
        photos.push(imageUrl);
    });
    displayImage(photos[0]);
  })
}

function genURL (photoObj) {
    return "https://farm" + photoObj.farm +
            ".staticflickr.com/" + photoObj.server +
            "/" + photoObj.id + "_" + photoObj.secret + ".jpg";
}

function displayImage(photoUrl){    
    let photoDiv=document.getElementById('photo');
    photoDiv.innerHTML="";
    let imgElement=document.createElement('img');
    imgElement.setAttribute('src',photoUrl);
    photoDiv.appendChild(imgElement);

    let showingLabel=document.getElementById('showingLabel');
    const labeltext=`Showing ${currentIndex+1} of ${photos.length}`;
    showingLabel.innerText=labeltext;
}

function showNextImage(){
    if(currentIndex===4){
        currentIndex=0;
    }
    else{
        currentIndex++;
    }
    displayImage(photos[currentIndex]);
}
//need to add a fliter feature still working on this part
function filterImage () {

}
yourLocation();
